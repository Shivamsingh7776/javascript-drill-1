const car = require('./Problem-2');
const inventory = require('./inventory');
const answer = [
    {
      id: 50,
      car_make: 'Lincoln',
      car_model: 'Town Car',
      car_year: 1999
    }
  ];
test("Last car is:", () =>{
    expect(car(inventory)).toStrictEqual(answer);
})