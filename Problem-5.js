var car = require('./inventory');


function older_car(inventory){
    let ans = [];
    for(let row = 0; row < inventory.length; row++){
        if(inventory[row].car_year > 2000){
            ans.push(inventory[row]);
        }
    }
    return ans;
}
let answer = older_car(car);
console.log(answer.length);
module.exports = older_car;